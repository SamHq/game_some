﻿#include "game_objects.h"

#include <iostream>
#include <vector>
#include <thread>       // std::this_thread::sleep_for
#include <chrono>       // std::chrono

#include <SFML/Graphics.hpp>

#include "inc/game_objects.h"

using namespace game_objects;

const int win_size_x = 900;
const int win_size_y = 600;

int main()
{
    double time = 0;
    const int win_center_x = win_size_x / 2;
    const int win_center_y = win_size_y / 2;

    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(win_size_x, win_size_y), "Planets");
    // Подгрузка фонового изображения
    sf::Texture texture;
    if (!texture.loadFromFile("img/fon.png"))
    {
        std::cout << "ERROR when loading fon.png" << std::endl;
        return false;
    }
    sf::Sprite fon_sprite;
    fon_sprite.setTexture(texture);


    // Добавление иконки
    sf::Image icon;
    if (!icon.loadFromFile("img/icon.png"))
    {
        return 1;
    }
    window.setIcon(32, 32, icon.getPixelsPtr());


    //// Добавим список объектов
    std::vector<Planet*> objects;
    objects.push_back(new Planet(0, 0, win_center_x, win_center_y, 0));
    if (!objects.back()->init("img/sun.png", 0.5))
        return 1;

    objects.push_back(new Planet(300, 220, win_center_x, win_center_y, 1));
    if (!objects.back()->init("img/planet1.png", 0.2))
        return 1;

    objects.push_back(new Planet(120, 140, win_center_x, win_center_y, 1.8));
    if (!objects.back()->init("img/planet2.png", 0.1))
        return 1;
    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Добавляем контроль событий
        sf::Event event;

        // Вывод на экрна
        window.clear();

        // Вывод фона
        window.draw(fon_sprite);

        for (size_t i = 0; i < objects.size(); i++)
        {
            objects[i]->draw(time, &window);
        }

        // Отобразить на окне все, что есть в буфере
        window.display();

        // Контролируем событие закрытия окна
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // https://ravesli.com/urok-129-tajming-koda-vremya-vypolneniya-programmy/
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        time += 0.01;
    }
    // очистка памяти
    for (size_t i = 0; i < objects.size(); i++)
    {
        delete objects[i];
    }
}